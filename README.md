M3UPorterLarge
=========

Copy M3U playlists to a single directory for car, receiver, or whatever.
Small, easy to use, and runs on Windows 7, or any platform that has .NET 4.5.

PorterLarge is a new-features fork of [hexblot/M3UPorter](https://github.com/hexblot/M3UPorter).
Most of the fixes and features have been merged with the original project.

Uses:

 * Copying MP3s to USB for your car, receiver, alarm clock, etc.
 * Moving files into a flat directory structure
 * Copying a playlist to a single directory

Features:
 * Flattens playlist by copying files to a single folder. 
 * Can optionally prefix the file names with a number, to play songs in order
 * Can move files instead of copying
 * Will stop copying when the output device is full
 * Skips missing input files
 * Can handle large playlists

"Build on an afternoon of frustration because I just couldn't find a tool to do 
this that worked on Windows 7." --hexblot

You can find a compiled version of the app on the Releases tab above.

Manual
=========

1. Select playlist file by dragging and dropping on form or the load button
2. Select output directory or device
3. Select options
  * Prefix with playlist position will make your files be prefixed with 001 - myfile.mp3, 002 - myotherfile.mp3 etc
  * Move instead of copy - will move yoru files, so use with care!
4. Click Go!
